Setting up Rietveld-Django
==========================

Set up a virtual environment
----------------------------

::

    ~/src$ virtualenv rdjango
    ~/src$ source rdjango/bin/activate

Download Django
---------------

::

    (rdjango) ~/src$ wget http://media.djangoproject.com/releases/1.3/Django-1.3.1.tar.gz

Install Django into the virtual environment
-------------------------------------------

::

    (rdjango) ~/src$ tar xzf Django-1.3.1.tar.gz
    (rdjango) ~/src$ cd Django-1.3.1
    (rdjango) ~/src/Django-1.3.1$ python setup.py install
    (rdjango) ~/src/Django-1.3.1$ cd ..

Clone (or untar) rietveld_django & setup
-----------------------------------------------------------

::

    (rdjango) ~/src$ git clone git@bitbucket.org:rick446/rietveld_django.git rietveld_django

OR

::

    (rdjango) ~/src$ wget https://bitbucket.org/rick446/rietveld_django/downloads/RietveldDjango-0.1.tar.gz
    (rdjango) ~/src$ tar xzf RietveldDjango-0.1.tar.gz
    (rdjango) ~/src$ ln -s RietveldDjango-0.1 rietveld_django

Clone django-gae2django & setup
-------------------------------

::

    (rdjango) ~/src$ hg clone http://code.google.com/p/django-gae2django django-gae2django
    (rdjango) ~/src$ cd django-gae2django
    (rdjango) ~/src/django-gae2django$ patch -p1 < ../rietveld_django/add-email-support.diff
    (rdjango) ~/src/django-gae2django$ patch -p1 < ../rietveld_django/fix-blob-field.diff
    (rdjango) ~/src/django-gae2django$ patch -p1 < ../rietveld_django/fix-p4.diff
    (rdjango) ~/src/django-gae2django$ patch -p1 < ../rietveld_django/make-gae-fields-uneditable.diff
    (rdjango) ~/src/django-gae2django$ pip install -e ./ 
    (rdjango) ~/src/django-gae2django$ cd ..

Run the tests
----------------------

::

    (rdjango) ~/src$ cd rietveld_django
    (rdjango) ~/src/rietveld_django$ python manage.py test codereview


Sync the database, creating the admin user, and run the server
------------------------------------------------------------------------------------------

::

    (rdjango) ~/src$ cd rietveld_django
    (rdjango) ~/src/rietveld_django$ ./manage.py syncdb
    (rdjango) ~/src/rietveld_django$ ./manage.py runserver 

Upload a patch
==============

Go to some p4 client workspace. Let's call it 'myproject' for now.

::

    (rdjango) ~/src/myproject$ p4 edit README
    (rdjango) ~/src/myproject$ vi README 
    (rdjango) ~/src/myproject$ p4 change
    (rdjango) ~/src/myproject$ ~/src/rietveld_django/upload.py --vcs p4 --p4_changelist 4 -s localhost:8000

Now you can edit the newly created issue on the server. Maybe you need to make some 
more changes based on feedback. So just upload a new patch to the same issue.

::

    (rdjango) ~/src/myproject$ vi README 
    (rdjango) ~/src/myproject$ ~/src/rietveld_django/upload.py --vcs p4 --p4_changelist 4 -s localhost:8000 -i 15 # issue number

Run the Incoming Mail Server
================================

::

    (rdjango) ~/src$ cd rietveld_django
    (rdjango) ~/src/rietveld_django$ pip install -e ./
    (rdjango) ~/src/rietveld_django$ smtp-server
